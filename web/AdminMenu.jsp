<%-- 
    Document   : AdminMenu
    Created on : Mar 20, 2024, 3:47:03 PM
    Author     : Asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
    <script src="js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/adminMenu.css">
    
</head>



    <div class="main-container d-flex">
        <div class="sidebar" id="side_nav">
            <div class="header-box px-2 pt-3 pb-4 d-flex justify-content-between">
                <a href="home" id="homeLink2"><img class="logo" style="height:50px" src="img/logo.jpg"></a>
                <button class="btn  d-block close-btn px-1 py-0">&times;</button>
            </div>

            <ul class="list-unstyled px-2">
                <li class="active"><a class="text-decoration-none px-3 py-2 d-block" href="crudacc" id="manageAccountLink">Quản lý tài khoản</a></li>
                <li class="">
                    <a class="text-decoration-none px-3 py-2 d-block" href="crudtutor" id="acceptTutorLink">
                            Xác thực Tutor 
                            <b><span id="amountPending" style="
                                     background-color: red;
                                     color: white;
                                     border: 2px solid black;
                                     border-radius: 50%;
                                     width: 23px;
                                     height: 23px;
                                     display: inline-block;
                                     text-align: center;
                                     line-height: 20px;
                                     vertical-align: middle; /* Thêm dòng này để căn giữa theo chiều dọc */
                                     margin-top: -1px; /* Điều chỉnh margin-top nếu cần thiết */
                                     "></span></b>
                        </a>
                </li>
                <li class=""><a href="#" class="text-decoration-none px-3 py-2 d-block d-flex justify-content-between">
                        <a class="text-decoration-none px-3 py-2 d-block" href="ManageClass?xd=0" type="button"  >
                            Quản lý của admin
                            <b><span id="amountRequestAll" class="amount"></span></b>
                        </a>      
                </li>
           
            </ul>
            <hr class="h-color mx-2">

            <ul class="list-unstyled px-2">
                <li class=""><a href="#" class="text-decoration-none px-3 py-2 d-block d-flex justify-content-between">
                        <a class="text-decoration-none px-3 py-2 d-block" href="ManageClass?xd=0" type="button"  >
                            Quản lý của admin
                            <b><span id="amountRequestAll" class="amount"></span></b>
                        </a>      
                </li>

            </ul>

        </div>
     
            <nav class="navbar navbar-expand-md navbar-light bg-light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between d-block">
                        <button class="btn px-1 py-0 open-btn me-2" ">abc</i></button>

                    </div>
                    <button class="navbar-toggler p-0 border-0" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fal fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                        <ul class="navbar-nav mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Profile</a>
                            </li>

                        </ul>

                    </div>
                </div>
            </nav>

            
    
    </div>
<script src="js/AdminMenu.js"></script>
    
