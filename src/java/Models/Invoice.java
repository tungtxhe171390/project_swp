/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.sql.Timestamp;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author mim
 */
public class Invoice {
    private int invoiceID;
    private int requestID;
    private Date createDate;
    private int invoiceType;
    private String imgInvoice;
    private double ammount;
    private boolean hasPaid;
    private Timestamp time;

    public Invoice() {
    }

    public Invoice(int invoiceID, int requestID, Date createDate, int invoiceType, String imgInvoice, double ammount) {
        this.invoiceID = invoiceID;
        this.requestID = requestID;
        this.createDate = createDate;
        this.invoiceType = invoiceType;
        this.imgInvoice = imgInvoice;
        this.ammount = ammount;

    }

    public double getAmmount() {
        return ammount;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

    

    public int getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(int invoiceID) {
        this.invoiceID = invoiceID;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(int invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getImgInvoice() {
        return imgInvoice;
    }

    public void setImgInvoice(String imgInvoice) {
        this.imgInvoice = imgInvoice;
    }

    public boolean isHasPaid() {
        return hasPaid;
    }

    public void setHasPaid(boolean hasPaid) {
        this.hasPaid = hasPaid;
    }

    
    public String getDateString(){
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String formattedCreateTime = dateFormat.format(time);
        return formattedCreateTime.format(formattedCreateTime);
    }
    
    public String getFormatedPrice(){
        NumberFormat formatter = new DecimalFormat("#,##0.00");;
        return formatter.format(this.ammount)+"đ";
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
    
}
