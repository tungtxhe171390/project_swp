/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAL.RequestDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class SendClassRequestController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int classID = Integer.parseInt(request.getParameter("classID"));
        int tutorID = Integer.parseInt(request.getParameter("tutorID"));
        RequestDAO requestDAO = new RequestDAO();
        int result = 0;
        try {
            //check if this class has been applied by this tutor before.
            if (requestDAO.checkClassRequest(tutorID, classID) > 0) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.setContentType("text/plain");
                response.getWriter().write("Bạn đã nhận lớp này, xin vui lòng kiểm tra lại yêu cầu của bạn.");
                return;
            }
            result = requestDAO.createClassRequest(tutorID, classID);
            if (result > 0) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.setContentType("text/plain");
                response.getWriter().write("Bạn có yêu cầu đang đợi xét duyệt!");
            }
        } catch (SQLException ex) {
            if (result == 0) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            Logger.getLogger(SendClassRequestController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SendClassRequestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("classid: " + classID);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String classID = request.getParameter("classID");
        System.out.println("classid: " + classID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
