/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author mim
 */
public class Request {

    private int requestID;
    private int classID;
    private int tutorID;
    private int requestType;
    private Date dateCreate;
    private int requestStatus;
    private Date trialEndDate;
    private Date acceptDate;
    private String contractLink;
    
    private ArrayList<Invoice> invoiceList;

    public Request() {
    }

    public Request(int requestID, int classID, int tutorID, int requestType, Date dateCreate, int requestStatus, Date trialEndDate, Date acceptDate, String contractLink) {
        this.requestID = requestID;
        this.classID = classID;
        this.tutorID = tutorID;
        this.requestType = requestType;
        this.dateCreate = dateCreate;
        this.requestStatus = requestStatus;
        this.trialEndDate = trialEndDate;
        this.acceptDate = acceptDate;
        this.contractLink = contractLink;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public int getTutorID() {
        return tutorID;
    }

    public void setTutorID(int tutorID) {
        this.tutorID = tutorID;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public String getDateCreateString() {
        SimpleDateFormat sdf = new SimpleDateFormat(
                "dd/MM/yyyy");
        return sdf.format(dateCreate);
    }
    
    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(int requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Date getTrialEndDate() {
        return trialEndDate;
    }

    public void setTrialEndDate(Date trialEndDate) {
        this.trialEndDate = trialEndDate;
    }

    public Date getAcceptDate() {
        return acceptDate;
    }
    
    public String getAcceptDateString(){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "dd/MM/yyyy");
        return sdf.format(acceptDate);
    }

    public void setAcceptDate(Date acceptDate) {
        this.acceptDate = acceptDate;
    }

    public String getContractLink() {
        return contractLink;
    }

    public void setContractLink(String contractLink) {
        this.contractLink = contractLink;
    }

    public ArrayList<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(ArrayList<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }
    
    
}
