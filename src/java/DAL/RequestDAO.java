/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Models.Invoice;
import Models.Request;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class RequestDAO {

    public List<Models.Request> getTutorRequest(int tutorID, String classID, String MMYy, String status) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String getRequest = "SELECT [RequestID], [ClassID], [TutorID], [RequestType], [DateCreate], [RequestStatus], [TrialEndDate], [AcceptDate], [ContractLink]  "
                + "From Request Where TutorID = ? ORDER BY DateCreate DESC";
        List<Models.Request> list = new ArrayList<>();
        
        String getInvoice = "SELECT [InvoiceID], [RequestID], [CreateTime], [InvoiceType], [Ammount] FROM Invoice WHERE RequestID=?";
        PreparedStatement invoicePS = null;
        ResultSet invoiceRS = null;
        try {
            con = new DBContext().getConnection();
            ps = con.prepareStatement(getRequest);
            ps.setInt(1, tutorID);
            rs = ps.executeQuery();
            
            invoicePS = con.prepareStatement(getInvoice);
            while (rs.next()) {
                if (!rs.getString("ClassID").contains(classID)){
                    continue;
                }
                if (!rs.getString("RequestStatus").contains(status)){
                    continue;
                }
                
                Date date = rs.getDate("DateCreate");
                String createDate = sdf.format(date);
                System.out.println(rs.getString("ContractLink"));
                if (!this.getMMYy(createDate).contains(MMYy)){
                    continue;
                }
                
                Request request = new Request(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getDate(5),
                        rs.getInt(6),
                        rs.getDate(7),
                        rs.getDate(8),
                        rs.getString(9)
                );
                
                invoicePS.setInt(1, rs.getInt("RequestID"));
                invoiceRS = invoicePS.executeQuery();
                ArrayList<Invoice> invoices = new ArrayList<>();
                while (invoiceRS.next()){
                    System.out.println(invoiceRS.getInt("InvoiceID"));
                    Invoice invoice = new Invoice();
                    invoice.setInvoiceID(invoiceRS.getInt("InvoiceID"));
                    invoice.setRequestID(invoiceRS.getInt("RequestID"));
                    invoice.setCreateDate(invoiceRS.getDate("CreateTime"));
                    invoice.setInvoiceType(invoiceRS.getInt("InvoiceType"));
                    invoice.setAmmount(invoiceRS.getDouble("Ammount"));
                    invoice.setTime(invoiceRS.getTimestamp("CreateTime"));
                    System.out.println(invoiceRS.getTimestamp("CreateTime"));
                    
                    invoices.add(invoice);
                }
                if (!invoices.isEmpty()){
                    request.setInvoiceList(invoices);
                }
                
                
                list.add(request);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResource(con);
            closeResource(ps);
        }
        return list;
    }
    
    private String getMMYy(String date){
        String[] splittedDate = date.split("/",2);
        return splittedDate[1];
    }

    public ArrayList<String> getCreateDate(int tutorID) {
        ArrayList<String> dateList = new ArrayList<>();
        Connection conn = null;
        String getQuery = "SELECT DISTINCT CONCAT(MONTH(dateCreate), '/', YEAR(dateCreate)) AS month_year\n"
                + "FROM Request WHERE TutorID = ?;";
        PreparedStatement getPS = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().getConnection();
            getPS = conn.prepareStatement(getQuery);
            getPS.setInt(1, tutorID);
            rs = getPS.executeQuery();
            while (rs.next()) {
                dateList.add(rs.getString("month_year"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(TutorDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeResource(conn);
            closeResource(getPS);
        }
        return dateList;
    }

    public int countRequest(int tutorID) throws Exception {
        Connection conn = null;
        String countQuery = "SELECT COUNT(RequestID) as RequestID FROM Request Where TutorID = ? AND RequestStatus = ?";
        PreparedStatement countPS = null;
        ResultSet countRS = null;
        try {
            conn = new DBContext().getConnection();
            countPS = conn.prepareStatement(countQuery);
            countPS.setInt(1, tutorID);
            countPS.setInt(2, 0);
            countRS = countPS.executeQuery();
            if (countRS.next()) {
                return countRS.getInt("RequestID");
            }

        } catch (SQLException ex) {
            Logger.getLogger(TutorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        } finally {
            closeResource(conn);
            closeResource(countPS);
        }
        return -1;
    }

    public int checkClassRequest(int tutorID, int classID) throws Exception {
        Connection conn = null;
        String countQuery = "SELECT COUNT(RequestID) as RequestID FROM Request Where TutorID = ? AND ClassID = ?";
        PreparedStatement countPS = null;
        ResultSet countRS = null;
        try {
            conn = new DBContext().getConnection();
            countPS = conn.prepareStatement(countQuery);
            countPS.setInt(1, tutorID);
            countPS.setInt(2, classID);
            countRS = countPS.executeQuery();
            if (countRS.next()) {
                System.out.println("oyeah: " + countRS.getInt("RequestID"));
                return countRS.getInt("RequestID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(TutorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        } finally {
            closeResource(conn);
            closeResource(countPS);
        }
        return -1;
    }

    public int createClassRequest(int tutorID, int classID) throws SQLException {
        Connection conn = null;
        String insertQuery = "INSERT INTO Request(ClassID,  TutorID, RequestType, DateCreate, RequestStatus) Values (?,?,0, GETDATE(), 0)";
        String setClassTutor = "UPDATE Class SET TutorID = ? WHERE ClassID = ?";
        PreparedStatement insertPS = null;
        PreparedStatement updateClass = null;
        int rowsUpdated = 0;
        try {
            if (this.countRequest(tutorID) > 0) {
                return 0;
            }
            conn = new DBContext().getConnection();
            insertPS = conn.prepareStatement(insertQuery);
            insertPS.setInt(1, classID);
            insertPS.setInt(2, tutorID);
            rowsUpdated = insertPS.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TutorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return rowsUpdated;
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeResource(conn);
            closeResource(insertPS);
        }
        return rowsUpdated;
    }
    
    public int cancelClassRequest(int classID, int tutorID){
        Connection con =null;
        String setStatusQuery = "UPDATE Request SET [RequestStatus] = 2 WHERE ClassID = ? AND TutorID = ?";
        PreparedStatement ps = null;
        int rowsUpdated = 0;
        try {
            con = new DBContext().getConnection();
            ps = con.prepareStatement(setStatusQuery);
            ps.setInt(1, classID);
            ps.setInt(2, tutorID);
            rowsUpdated = ps.executeUpdate();
            return rowsUpdated;
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeResource(con);
            closeResource(ps);
        }
        return rowsUpdated;
    }
    
    public int createInvoice(int requestID, double price){
        Connection conn = null;
        String insertQuery = "INSERT INTO Invoice(RequestID, CreateTime, InvoiceType, Ammount) Values (?, GETDATE(), 0, ?)";
        PreparedStatement ps = null;
        int rowsUpdated = 0;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(insertQuery);
            ps.setInt(1, requestID);
            ps.setDouble(2, price*40/100);
            rowsUpdated = ps.executeUpdate();
            return rowsUpdated;
        } catch (Exception ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeResource(conn);
            closeResource(ps);
        }
        return rowsUpdated;
    }

    private void closeResource(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void closeResource(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
