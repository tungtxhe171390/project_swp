<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Studium</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Raleway:wght@100;600;800&display=swap" rel="stylesheet"> 

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/chosen.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"/>
        <link href="css/style.css" rel="stylesheet">
        <link href="css/info.css" rel="stylesheet">

    </head>

    <body style="min-height: 100vh;">
        <jsp:include page="Menu.jsp"></jsp:include> 

            <div class="container my-5">
                <div class="row gutters">
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="account-settings">
                                    <div class="user-profile">
                                        <div class="user-avatar">
                                            <img src="img/${userObj.profilePic}" alt="${userObj.getProfilePic()}'s IMG">
                                    </div>
                                    <h5 class="user-name">${userObj.userName}</h5>
                                    <h6 class="user-email">${userObj.email}</h6>
                                </div>
                                <c:if test="${userObj.role == 2}">
                                    <jsp:include page="TutorMenu.jsp"></jsp:include>  
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="card h-100">
                        <div class="card-body">
                            <h3 class="fw-bolder">Yêu cầu nhận lớp</h3>
                            <form action="classrequestmanage" method="get">

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="classID">Mã lớp</label>
                                        <input id="classID" value="${param["classID"]}" name="classID" class="form-control" type="text" placeholder="" onfocus="this.value = ''">
                                        <spann style="position: absolute; right: 0">&times;</spann>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="date">Thời gian tạo yêu cầu</label>
                                        <select id="date" name="date" class="form-control">
                                            <option value="" ${param["date"]==""?"selected":""} >Thời gian tạo</option>
                                            <c:forEach items="${dateList}" var="date">
                                                <option value="${date}" ${param["date"]==date?"selected":""} >Tháng ${date}</option>
                                            </c:forEach>

                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="locationSelect">Trạng thái</label>
                                        <div class="input-group">
                                            <select id="status" name="status" class="form-control">
                                                <option value="" ${param["status"]==""?"selected":""} >Trạng thái yêu cầu</option>
                                                <option value="0" ${param["status"]=="0"?"selected":""} >Đang đợi duyệt</option>
                                                <option value="1" ${param["status"]=="1"?"selected":""}>Đã được chấp thuận </option>
                                                <option value="2" ${param["status"]=="2"?"selected":""}>Đã hủy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="padding-top: 20px">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fas fa-search text-white"></i>
                                        </button>
                                    </div>
                                </div>

                            </form>
                            <table class="table mb-5">
                                <thead>
                                    <tr >                                     
                                        <th><span>ID lớp</span></th>
                                        <th><span>Ngày tạo yêu cầu</span></th>
                                        <th><span>Tình trạng</span></th>
                                        <th><span>Ngày duyệt</span></th>
                                        <th><span>Hóa đơn</span></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="requestContainer">
                                    <c:if test="${requestList != null}">
                                        <c:set var="counter" value="1" />
                                        <c:forEach items="${requestList}" var="rl">
                                            <tr id="${counter}">
                                                <td><a href='#'>Mã lớp ${rl.classID}</a></td>
                                                <td>${rl.getDateCreateString()}</td>
                                                <td class="rqStatus">
                                                    <c:if test="${rl.requestStatus == 0}">Đang đợi duyệt</c:if>
                                                    <c:if test="${rl.requestStatus == 1}"><span style="color: #0ce81e;">Đã được duyệt</span></c:if>
                                                    <c:if test="${rl.requestStatus == 2}"><span style="color: red;">Đã bị hủy</span></c:if>
                                                    </td>
                                                    <td>
                                                    <c:if test="${rl.acceptDate == Null}">Chưa có</c:if>
                                                    <c:if test="${rl.acceptDate != Null}">${rl.getAcceptDateString()}</c:if>
                                                    </td>
                                                    <td>
                                                    <c:if test="${rl.acceptDate == Null}">Chưa có</c:if>
                                                    <c:if test="${rl.acceptDate != Null && rl.invoiceList == Null}"><Span class="text-danger">Chưa thanh toán</span></c:if>
                                                    <c:if test="${rl.acceptDate != Null && rl.invoiceList != Null}"><Span style="color: #0ce81e;">Đã thanh toán</span></c:if>
                                                    </td>
                                                <c:if test="${rl.requestStatus == 0}">

                                                    <td>
                                                        <button 
                                                            class='btn btn-danger delete-button' data-toggle="modal" data-target="#largeModal${counter}"  onclick="cancelRequest(${userObj.id},${rl.classID},${counter});">
                                                            Hủy yêu cầu
                                                        </button>
                                                    </td>

                                                </c:if>
                                                <c:if test="${rl.requestStatus == 1}">
                                                    <c:if test="${rl.invoiceList == null}">
                                                        <td>
                                                            <form action="payment" method="POST">
                                                                <input type="hidden" name="sendClassID" value="${rl.requestID}"/>
                                                                <button class='btn btn-success text-white' >
                                                                    Thanh toán
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </c:if>
                                                    <c:if test="${rl.invoiceList != null}">
                                                        <td>
                                                            <button class='btn btn-primary text-white' data-toggle="modal" data-target="#detailModal${counter}" >
                                                                Xem chi tiết
                                                            </button>
                                                        </td>
                                                    </c:if>
                                                </c:if>
                                                <c:if test="${rl.requestStatus == 2}">

                                                    <td>
                                                        <button class='btn btn-primary text-white' data-toggle="modal" data-target="#detailModal${counter}" <c:if test="${rl.invoiceList == null}">disabled</c:if>>
                                                                Xem chi tiết
                                                            </button>
                                                        </td>

                                                </c:if>
                                        <div class="modal fade" id="largeModal${counter}" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                            <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" " id="myModalLabel" class="text-danger">Chú ý!</h4>
                                                    </div>
                                                    <div class="modal-body px-4">
                                                        <span class="tw-bold">Sau khi hủy lớp bạn sẽ không thể nhận lại lớp này!</span>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="tutorID" value="${userObj.id}">  
                                                        <input type="hidden" name="classID" value="${ClassList.classID}">
                                                        <button type="button" class="btn btn-outline-secondary background" data-dismiss="modal">Quay lại</button>
                                                        <button id="acceptButton" type="submit" class="btn btn-danger text-white" data-dismiss="modal" onclick="cancelRequest(${userObj.id},${rl.classID},${counter});">Hủy lớp</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="detailModal${counter}" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                            <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" " id="myModalLabel">Chi tiết yêu cầu</h4>
                                                    </div>
                                                    <div class="modal-body px-4">
                                                        <span class="tw-bold">Link hợp đồng: </span><a target=”_blank” href="${rl.contractLink}">${rl.contractLink}</a><br/>
                                                        <c:forEach items="${rl.invoiceList}" var="invoice">
                                                            <h6 class="modal-title mt-3" " id="myModalLabel" class="text-danger">Hóa đơn:</h6>
                                                            <div>
                                                                <span class="tw-bold">Mã hóa đơn: </span>${invoice.invoiceID}<br/>
                                                                <span class="tw-bold">Ngày tạo hóa đơn: </span>${invoice.getDateString()}</a><br/>
                                                                <span class="tw-bold">Số tiền: </span>${invoice.getFormatedPrice()}</a><br/>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary background" data-dismiss="modal">Quay lại</button>
<!--                                                        <button id="acceptButton" type="submit" class="btn btn-danger text-white" data-dismiss="modal" onclick="cancelRequest(${userObj.id},${rl.classID},${counter});">Hủy lớp</button>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </tr>
                                        <c:set var="counter" value="${counter + 1}" />
                                    </c:forEach>
                                </c:if>
                                </tbody>
                            </table >
                            <div class="pagination" style="position: absolute; bottom: 0;">
                                <ul class="pagination">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script>
                                                            document.getElementById("requestMenu").classList.add('active');

                                                            const itemPerPage = 10;
                                                            const requestContainer = document.getElementById('requestContainer');
                                                            const totalPages = Math.ceil(${requestList.size()} / itemPerPage);

                                                            function showPage(page) {
                                                                const startIdx = (page - 1) * itemPerPage;
                                                                const endIdx = startIdx + itemPerPage;

                                                                for (let i = 0; i < requestContainer.children.length; i++) {
                                                                    if (i >= startIdx && i < endIdx) {
                                                                        requestContainer.children[i].classList.remove('d-none');
                                                                    } else {
                                                                        requestContainer.children[i].classList.add('d-none');
                                                                        ;
                                                                    }
                                                                }
                                                            }

                                                            function populatePagination() {
                                                                const paginationList = document.querySelector('.pagination');
                                                                paginationList.innerHTML = '';

                                                                for (let i = 1; i <= totalPages; i++) {
                                                                    const listItem = document.createElement('li');
                                                                    listItem.classList.add('page-item');
                                                                    const link = document.createElement('a');
                                                                    link.classList.add('page-link');
                                                                    link.href = '#';
                                                                    link.textContent = i;
                                                                    link.onclick = () => showPage(i);
                                                                    listItem.appendChild(link);
                                                                    paginationList.appendChild(listItem);
                                                                }
                                                            }


                                                            showPage(${param["pageNume"]==""?param["pageNume"]:1});
                                                            populatePagination();


                                                            function cancelRequest(a, b, c) {
                                                                $.ajax({
                                                                    url: "classrequestmanage",
                                                                    type: "POST",
                                                                    data: {
                                                                        tutorID: a,
                                                                        classID: b
                                                                    },
                                                                    success: function (response) {
                                                                        console.log("ok");
                                                                        const parent = document.getElementById(c);
                                                                        const rqStatus = parent.querySelector(".rqStatus");
                                                                        const rqButton = parent.querySelector(".delete-button");
                                                                        rqStatus.innerHTML = "<span class='text-danger'>Đã bị hủy</span>";
                                                                        rqButton.setAttribute("disabled", "");
                                                                        console.log("Lớp đã được hủy.");
                                                                    },
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        console.error("Error:", textStatus, errorThrown);
                                                                        $("#result").html("Error occurred during request.");
                                                                    }
                                                                });
                                                            }

                                                            if (window.history.replaceState) {
                                                                window.history.replaceState(null, null, window.location.href);
                                                            }

        </script>
    </body>
    <jsp:include page="Footer.jsp"></jsp:include>   
</html>
