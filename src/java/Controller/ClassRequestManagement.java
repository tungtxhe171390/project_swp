/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAL.RequestDAO;
import Models.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author Asus
 */
@WebServlet(name = "ClassRequestManagement", urlPatterns = {"/classrequestmanage"})
public class ClassRequestManagement extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDAO requestDAO = new RequestDAO();
        HttpSession session = request.getSession();
        Account acc = (Account) session.getAttribute("userObj");
        String classID = request.getParameter("classID") == null ? "" : request.getParameter("classID");
        String monthAndYear = request.getParameter("date") == null ? "" : request.getParameter("date");
        String status = request.getParameter("status") == null ? "" : request.getParameter("status");

        List<Models.Request> requestList = requestDAO.getTutorRequest(acc.getId(), classID, monthAndYear, status);
        List<String> dateList = requestDAO.getCreateDate(acc.getId());

        request.setAttribute("requestList", requestList);
        request.setAttribute("dateList", dateList);
        request.getRequestDispatcher("TutorClassRequest.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getParameter("sendClassID")!=null){
            doGet(request, response);
        }
        int tutorID = 0;
        int classID = 0;
        try {
            classID = Integer.parseInt(request.getParameter("classID"));
            tutorID = Integer.parseInt(request.getParameter("tutorID"));
        } catch (NumberFormatException ex){
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST); 
            response.setContentType("text/plain");
            response.getWriter().write("Đã xảy ra lỗi!");
            return;
        }

        System.out.println(request.getParameter("tutorID") + " " + request.getParameter("classID"));
        RequestDAO requestDAO = new RequestDAO();
        int rowsUpdated = requestDAO.cancelClassRequest(classID, tutorID);
        System.out.println(rowsUpdated);
        if(rowsUpdated == 1) {
            response.setStatus(HttpServletResponse.SC_OK); 
            return;
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST); 
            response.setContentType("text/plain");
            response.getWriter().write("Đã xảy ra lỗi!");
            return;
        }
    }

    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
